/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void} Any output is written via response object
 */
function displayItemInfo(request, response) {
    var infoName = request.getParameter('infoType');
    var itemId = request.getParameter('itemId');

    switch(infoName){
        case 'specs':
            itemSpecs(itemId,response);
            break;
        case 'included':
            includedItems(itemId,response);
            break;
        case 'download':
            downloads(itemId,response);
            break;
    }
}

function itemSpecs(itemId,response){
    var specTable = "<table class=\"specsTable\">";

    var cols = [ new nlobjSearchColumn('custrecord_item_spec_name_id'),
        new nlobjSearchColumn('custrecord_item_spec_name_value') ];

    var searchResults = nlapiSearchRecord('customrecord_item_spec', null,
        new nlobjSearchFilter('custrecord_spec_item_parent', null, 'anyof',itemId, null), cols);

    var sortedArray = new Array();

    // 2 Dimensional Array to hold all research results
    if (searchResults !== null) {
        for(var i = 0; i < searchResults.length; i++) {
            sortedArray[i] = [searchResults[i].getText('custrecord_item_spec_name_id'), searchResults[i].getValue('custrecord_item_spec_name_value')];
        }
    }
    // Sort the results
    sortedArray.sort();

    // Print out the results in alphabetical order
    if(searchResults !== null) {
        for ( var i = 0; i < searchResults.length; i++) {
            specTable += '<tr><td>' + sortedArray[i][0] + '</td><td>' + sortedArray[i][1] + '</td></tr>';
        }
    }

    specTable += "</table>";

    response.write("document.write('" + specTable + "')");
}

function includedItems(itemId,response){
    var ulText = '<ul>';

    var searchResults = nlapiSearchRecord('customrecord_includeditem',
        null,
        new nlobjSearchFilter('custrecord_included_item_field',null,'anyof',itemId,null),
        new nlobjSearchColumn('custrecord_included_value')
    );

    if(searchResults != null){
        for ( var i = 0; i < searchResults.length; i++) {
            ulText += '<li>' + searchResults[i].getValue('custrecord_included_value') + '</li>';
        }
    }

    ulText += '</ul>';
    response.write('document.write("' + ulText + '")');
}

function downloads(itemId,response){
    var filters = new Array();
    filters[0] = new nlobjSearchFilter('custrecord_download_item_parent',null,'anyof',itemId);
    filters[1] = new nlobjSearchFilter('isinactive',null,'is','F');

    var columns = new Array();
    columns[0] = new nlobjSearchColumn('custrecord_download_name');
    columns[1] = new nlobjSearchColumn('custrecord_download_file');

    var searchResults = nlapiSearchRecord('customrecord_item_download', null, filters, columns);

    var productName =  nlapiLookupField('inventoryitem', itemId, 'itemid');

    var ulText = '<ul id="downloads">';

    if(searchResults != null){
        for(var i=0; i < searchResults.length; i++){
            var file = nlapiLoadFile(searchResults[i].getValue('custrecord_download_file'));
            var result = searchResults[i].getText('custrecord_download_name');
            //var analytics = "onclick=\"ga(\"send\",\"event\",\"" + result + '\",\"' + productName + '\")"';
            ulText += '<li><a href="'+ file.getURL() + '" target="_blank" ' + ';>' + searchResults[i].getText('custrecord_download_name') + '</a></li>';
        }
    }

    ulText += '</ul>';
    response.write('document.write(\'' + ulText + '\')');
}